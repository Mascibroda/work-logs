'use strict';
var app = angular.module('WorkLogsApp', ['ngRoute']);

app.controller('myCtrl', function myCtrl($scope, workersService, issuesService, logsService, changeView) {
    $scope.view = changeView.view;
    $scope.goTo = changeView.goTo;

    $scope.workers = workersService;
    $scope.issues = issuesService;
    $scope.logs = logsService;

});
app.directive('issueTable', function(changeView, sortable) {
    return {
        template: '<table class="table table-hover">' +
            '<thead><tr>' +
                '<th class="sortable sort-{{sort.class(\'name\')}}" ng-click="sort.change(\'name\')">Name</th>' +
                '<th class="sortable sort-{{sort.class(\'created\')}}" ng-click="sort.change(\'created\')">Created</th>' +
                '<th class="sortable sort-{{sort.class(\'status\')}}" ng-click="sort.change(\'status\')">Status</th>' +
                '<th class="sortable sort-{{sort.class(\'timeWorked\')}}" ng-click="sort.change(\'timeWorked\')">Time worked</th>' +
            '</tr></thead>' +
            '<tbody>' +
                '<tr ng-repeat="issue in list | orderObjectBy:sort.type:sort.reverse" ng-click="goTo(\'issue\', issue)" class="status-{{issue.status | diacritics}}">' +
                    '<td data-th="Name">{{issue.name}}</td>' +
                    '<td data-th="Created" class="created">{{issue.created | date : "yyyy-MM-dd"}}</td>' +
                    '<td data-th="Status" class="status"><span>{{issue.status}}</span></td>' +
                    '<td data-th="Time worked">{{issue.timeWorked | duration}}</td>' +
                '</tr>' +
            '</tbody>' +
            '</table>',
        scope:{
            list: '='
        },
        link: function(scope, element, attr){
            scope.goTo = changeView.goTo;
            scope.sort = new sortable('name');
        }
    };
});
app.directive('logTable', function(changeView, sortable) {
    return {
        template: '<table class="table">' +
            '<thead><tr>' +
                '<th class="sortable sort-{{sort.class(\'name\')}}" ng-click="sort.change(\'name\')">Name</th>' +
                '<th class="sortable sort-{{sort.class(\'comment\')}}" ng-click="sort.change(\'comment\')">Comment</th>' +
                '<th class="sortable sort-{{sort.class(\'created\')}}" ng-click="sort.change(\'created\')">Created</th>' +
                '<th class="sortable sort-{{sort.class(\'time\')}}" ng-click="sort.change(\'time\')">Time worked</th>' +
            '</tr></thead>' +
            '<tbody>' +
                '<tr ng-repeat="item in list | orderBy:sort.type:sort.reverse">' +
                    '<td data-th="Name">{{item.name}}</td>' +
                    '<td data-th="Comment" title="{{item.comment}}">{{item.comment | limitTo : 20}}{{item.comment.length > 20 ? " ...":""}}</td>' +
                    '<td data-th="Created" class="created">{{item.created | date: "yyyy-MM-dd"}}</td>' +
                    '<td data-th="Time worked">{{item.time | duration}}</td>' +
                '</tr>' +
            '</tbody>' +
            '</table>',
        scope:{
            list: '='
        },
        link: function(scope, element, attr){
            scope.goTo = changeView.goTo;
            scope.sort = sortable('name');
        }
    };
});
app.directive('workerTable', function(changeView, sortable) {
    return {
        template: '<table class="table table-hover">' +
            '<thead><tr>' +
                '<th class="sortable sort-{{sort.class(\'name\')}}" ng-click="sort.change(\'name\')">Name</th>' +
                '<th class="sortable sort-{{sort.class(\'surname\')}}" ng-click="sort.change(\'surname\')">Surname</th>' +
                '<th class="sortable sort-{{sort.class(\'address\')}}" ng-click="sort.change(\'address\')">Address</th>' +
                '<th class="sortable sort-{{sort.class(\'timeWorked\')}}" ng-click="sort.change(\'timeWorked\')">Time worked</th>' +
            '</tr></thead>' +
            '<tbody>' +
                '<tr ng-repeat="worker in list | orderObjectBy:sort.type:sort.reverse" ng-click="goTo(\'worker\', worker)">' +
                    '<td data-th="Name">{{worker.name}}</td>' +
                    '<td data-th="Surname">{{worker.surname}}</td>' +
                    '<td data-th="Address">{{worker.address}}</td>' +
                    '<td data-th="Time worked">{{worker.timeWorked | duration}}</td>' +
                '</tr>' +
            '</tbody>' +
            '</table>',
        scope:{
            list: '='
        },
        link: function(scope, element, attr){
            scope.goTo = changeView.goTo;
            scope.sort = sortable('name');
        }
    };
});
app.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});
app.filter('diacritics', function () {
    return function (input) {
        return input.replace(/\s/g, '');
    };
});
app.filter('duration', function () {
    return function(milliseconds) {
        var seconds = Math.floor(milliseconds / 1000);
        var days = Math.floor(seconds / 86400);
        var hours = Math.floor((seconds % 86400) / 3600);
        var minutes = Math.floor(((seconds % 86400) % 3600) / 60);
        var dateTimeDurationString = '';
        if ((days > 0) && (hours === 0 && minutes === 0)) dateTimeDurationString += (days > 1) ? (days + ' days ') : (days + ' day ');
        if ((days > 0) && (hours > 0 || minutes > 0)) dateTimeDurationString += (days > 1) ? (days + ' days, ') : (days + ' day, ');
        if ((hours > 0) && (minutes > 0)) dateTimeDurationString += (hours > 1) ? (hours + ' hours, ') : (hours + ' hour, ');
        if ((hours > 0) && (minutes === 0)) dateTimeDurationString += (hours > 1) ? (hours + ' hours ') : (hours + ' hour ');
        if (minutes > 0) dateTimeDurationString += (minutes > 1) ? (minutes + ' minutes ') : (minutes + ' minute ');
        return dateTimeDurationString;
    };
});
app.service('api', function api(jsonWorkers, jsonLogs, jsonIssues){
    var data = {
        workers: jsonWorkers,
        issues: jsonIssues,
        logs: jsonLogs
    };

    function getItemsBy(table, col, value){
        var list = angular.copy(data[table]);
        var result = [];
        for(var i=0;i<list.length;i++){
            var el = list[i];
            if(typeof el[col] != 'undefined' && el[col] == value){
                result.push(el);
            }
        }
        return result;
    }

    function calcLogTime(col, value){
        var sum = 0;
        for(var i=0;i<data.logs.length;i++){
            var el = data.logs[i];
            if(el[col] == value){
                sum += el.time;
            }
        }
        return sum;
    }

    return {
        getData: function(name){
            if(data[name]){
                return angular.copy(data[name]);
            }else{
                console.error('table "'+ name +'" not exist');
                return [];
            }
        },
        getItemsBy: getItemsBy,
        calcLogTime: calcLogTime
    }

});
app.service('jsonIssues', function jsonIssues(){
    return [
        {
            "id": 0,
            "name": "consequat esse",
            "status": "todo",
            "created": 1269544866552
        },
        {
            "id": 1,
            "name": "proident dolore",
            "status": "done",
            "created": 1375883842162
        },
        {
            "id": 2,
            "name": "nulla ipsum",
            "status": "closed",
            "created": 1458093988099
        },
        {
            "id": 3,
            "name": "sint eiusmod",
            "status": "new",
            "created": 1468281959098
        },
        {
            "id": 4,
            "name": "cupidatat incididunt",
            "status": "closed",
            "created": 1331122056037
        },
        {
            "id": 5,
            "name": "ex sit",
            "status": "in progress",
            "created": 1358605915956
        },
        {
            "id": 6,
            "name": "minim aliquip",
            "status": "in progress",
            "created": 1393195918799
        },
        {
            "id": 7,
            "name": "sint proident",
            "status": "new",
            "created": 1378672376454
        },
        {
            "id": 8,
            "name": "eu aute",
            "status": "new",
            "created": 1353644182883
        },
        {
            "id": 9,
            "name": "elit aliquip",
            "status": "done",
            "created": 1379176791996
        },
        {
            "id": 10,
            "name": "sunt exercitation",
            "status": "todo",
            "created": 1428696592593
        },
        {
            "id": 11,
            "name": "ipsum dolore",
            "status": "closed",
            "created": 1456367454130
        },
        {
            "id": 12,
            "name": "in incididunt",
            "status": "in progress",
            "created": 1286270037573
        },
        {
            "id": 13,
            "name": "commodo ullamco",
            "status": "todo",
            "created": 1445349400712
        },
        {
            "id": 14,
            "name": "non sunt",
            "status": "new",
            "created": 1190659915364
        },
        {
            "id": 15,
            "name": "anim velit",
            "status": "in progress",
            "created": 1245838442429
        },
        {
            "id": 16,
            "name": "irure dolore",
            "status": "new",
            "created": 1211094800890
        },
        {
            "id": 17,
            "name": "sunt mollit",
            "status": "in progress",
            "created": 1200697726217
        },
        {
            "id": 18,
            "name": "excepteur cupidatat",
            "status": "done",
            "created": 1217538709434
        },
        {
            "id": 19,
            "name": "quis aute",
            "status": "todo",
            "created": 1246857345365
        },
        {
            "id": 20,
            "name": "cillum Lorem",
            "status": "closed",
            "created": 1232673543934
        },
        {
            "id": 21,
            "name": "ullamco deserunt",
            "status": "done",
            "created": 1416995638926
        },
        {
            "id": 22,
            "name": "excepteur incididunt",
            "status": "closed",
            "created": 1281576351768
        },
        {
            "id": 23,
            "name": "sit sunt",
            "status": "in progress",
            "created": 1371408462981
        },
        {
            "id": 24,
            "name": "commodo enim",
            "status": "new",
            "created": 1206556979276
        },
        {
            "id": 25,
            "name": "proident tempor",
            "status": "new",
            "created": 1304943998284
        },
        {
            "id": 26,
            "name": "deserunt labore",
            "status": "new",
            "created": 1347884243409
        },
        {
            "id": 27,
            "name": "sit ipsum",
            "status": "closed",
            "created": 1183690342483
        },
        {
            "id": 28,
            "name": "in eu",
            "status": "todo",
            "created": 1462295489469
        },
        {
            "id": 29,
            "name": "ex est",
            "status": "todo",
            "created": 1383755531234
        }
    ]
});
app.service('jsonLogs', function jsonWorkers(){
    return [
        {
            "id": 0,
            "id_worker": 9,
            "id_issue": 4,
            "comment": "aliqua tempor adipisicing sint velit",
            "created": 1363402796012,
            "time": 13575965
        },
        {
            "id": 1,
            "id_worker": 13,
            "id_issue": 9,
            "comment": "velit deserunt cillum tempor duis eu",
            "created": 1305194282786,
            "time": 15878280
        },
        {
            "id": 2,
            "id_worker": 5,
            "id_issue": 7,
            "comment": "mollit nostrud qui incididunt do",
            "created": 1284441702589,
            "time": 16429030
        },
        {
            "id": 3,
            "id_worker": 10,
            "id_issue": 6,
            "comment": "duis elit",
            "created": 1419036473916,
            "time": 843112
        },
        {
            "id": 4,
            "id_worker": 4,
            "id_issue": 0,
            "comment": "eiusmod et Lorem",
            "created": 1420573308824,
            "time": 16380632
        },
        {
            "id": 5,
            "id_worker": 8,
            "id_issue": 9,
            "comment": "ad ullamco elit velit non duis",
            "created": 1264512507653,
            "time": 9246655
        },
        {
            "id": 6,
            "id_worker": 4,
            "id_issue": 6,
            "comment": "aute eiusmod",
            "created": 1228836400936,
            "time": 26706896
        },
        {
            "id": 7,
            "id_worker": 7,
            "id_issue": 27,
            "comment": "minim officia eiusmod fugiat reprehenderit",
            "created": 1352491340997,
            "time": 19240039
        },
        {
            "id": 8,
            "id_worker": 6,
            "id_issue": 4,
            "comment": "eiusmod elit ad dolor minim",
            "created": 1202610835622,
            "time": 8645433
        },
        {
            "id": 9,
            "id_worker": 5,
            "id_issue": 19,
            "comment": "sint enim",
            "created": 1262217169639,
            "time": 10655884
        },
        {
            "id": 10,
            "id_worker": 9,
            "id_issue": 27,
            "comment": "irure laborum tempor voluptate ex",
            "created": 1181866045128,
            "time": 20924379
        },
        {
            "id": 11,
            "id_worker": 14,
            "id_issue": 2,
            "comment": "eu minim Lorem nulla labore minim",
            "created": 1214237328342,
            "time": 22149634
        },
        {
            "id": 12,
            "id_worker": 12,
            "id_issue": 29,
            "comment": "eiusmod in",
            "created": 1467200658626,
            "time": 16213715
        },
        {
            "id": 13,
            "id_worker": 1,
            "id_issue": 7,
            "comment": "nostrud aute id",
            "created": 1379321887532,
            "time": 15807337
        },
        {
            "id": 14,
            "id_worker": 10,
            "id_issue": 12,
            "comment": "est magna",
            "created": 1448123082550,
            "time": 22829026
        },
        {
            "id": 15,
            "id_worker": 5,
            "id_issue": 20,
            "comment": "eiusmod nulla elit amet irure amet",
            "created": 1198219013200,
            "time": 7771816
        },
        {
            "id": 16,
            "id_worker": 7,
            "id_issue": 3,
            "comment": "ipsum proident qui in",
            "created": 1318058662376,
            "time": 23172480
        },
        {
            "id": 17,
            "id_worker": 2,
            "id_issue": 16,
            "comment": "qui proident nostrud qui",
            "created": 1311478516527,
            "time": 18572404
        },
        {
            "id": 18,
            "id_worker": 3,
            "id_issue": 22,
            "comment": "cupidatat pariatur",
            "created": 1438394546333,
            "time": 13998348
        },
        {
            "id": 19,
            "id_worker": 6,
            "id_issue": 11,
            "comment": "cillum voluptate",
            "created": 1188204811466,
            "time": 710383
        },
        {
            "id": 20,
            "id_worker": 13,
            "id_issue": 28,
            "comment": "ad dolor in sunt aute aute",
            "created": 1464272673890,
            "time": 2767589
        },
        {
            "id": 21,
            "id_worker": 0,
            "id_issue": 16,
            "comment": "ut consequat minim ex occaecat adipisicing",
            "created": 1266506100532,
            "time": 13245570
        },
        {
            "id": 22,
            "id_worker": 12,
            "id_issue": 18,
            "comment": "consequat voluptate dolor sunt voluptate in",
            "created": 1180241288687,
            "time": 22444538
        },
        {
            "id": 23,
            "id_worker": 2,
            "id_issue": 6,
            "comment": "in qui",
            "created": 1423806979873,
            "time": 10421460
        },
        {
            "id": 24,
            "id_worker": 4,
            "id_issue": 5,
            "comment": "cupidatat nulla",
            "created": 1400618031247,
            "time": 17285261
        },
        {
            "id": 25,
            "id_worker": 8,
            "id_issue": 11,
            "comment": "velit consectetur nisi pariatur voluptate mollit",
            "created": 1371878682190,
            "time": 7672027
        },
        {
            "id": 26,
            "id_worker": 6,
            "id_issue": 24,
            "comment": "sint consectetur",
            "created": 1340385167462,
            "time": 26201314
        },
        {
            "id": 27,
            "id_worker": 11,
            "id_issue": 24,
            "comment": "nulla ut culpa excepteur",
            "created": 1238252904271,
            "time": 18287824
        },
        {
            "id": 28,
            "id_worker": 0,
            "id_issue": 8,
            "comment": "tempor eiusmod",
            "created": 1357900778724,
            "time": 15092416
        },
        {
            "id": 29,
            "id_worker": 6,
            "id_issue": 14,
            "comment": "ad aliquip labore exercitation deserunt",
            "created": 1218279561569,
            "time": 9555664
        },
        {
            "id": 30,
            "id_worker": 13,
            "id_issue": 27,
            "comment": "qui et cupidatat laborum aliquip anim",
            "created": 1462904354006,
            "time": 5469932
        },
        {
            "id": 31,
            "id_worker": 7,
            "id_issue": 27,
            "comment": "ullamco ad nostrud laborum",
            "created": 1250587062657,
            "time": 27678568
        },
        {
            "id": 32,
            "id_worker": 14,
            "id_issue": 2,
            "comment": "consequat sunt",
            "created": 1218678971421,
            "time": 16137961
        },
        {
            "id": 33,
            "id_worker": 14,
            "id_issue": 7,
            "comment": "anim aliqua nulla cillum cupidatat",
            "created": 1320205764747,
            "time": 13999707
        },
        {
            "id": 34,
            "id_worker": 8,
            "id_issue": 18,
            "comment": "exercitation deserunt",
            "created": 1292892250931,
            "time": 3048373
        },
        {
            "id": 35,
            "id_worker": 9,
            "id_issue": 11,
            "comment": "qui dolor do laboris elit enim",
            "created": 1206096440417,
            "time": 1584007
        },
        {
            "id": 36,
            "id_worker": 5,
            "id_issue": 8,
            "comment": "nostrud consectetur dolore dolore do",
            "created": 1253021700803,
            "time": 10856291
        },
        {
            "id": 37,
            "id_worker": 5,
            "id_issue": 14,
            "comment": "laboris eu proident mollit irure commodo",
            "created": 1428919781289,
            "time": 12323619
        },
        {
            "id": 38,
            "id_worker": 12,
            "id_issue": 18,
            "comment": "excepteur nostrud",
            "created": 1454722268145,
            "time": 7161225
        },
        {
            "id": 39,
            "id_worker": 11,
            "id_issue": 22,
            "comment": "ea nostrud",
            "created": 1324947961451,
            "time": 9060371
        },
        {
            "id": 40,
            "id_worker": 3,
            "id_issue": 0,
            "comment": "fugiat labore aliquip excepteur proident",
            "created": 1270933363816,
            "time": 8878772
        },
        {
            "id": 41,
            "id_worker": 3,
            "id_issue": 16,
            "comment": "id commodo incididunt dolor voluptate",
            "created": 1281436783876,
            "time": 1552167
        },
        {
            "id": 42,
            "id_worker": 8,
            "id_issue": 6,
            "comment": "ut sint et",
            "created": 1326757111543,
            "time": 18612395
        },
        {
            "id": 43,
            "id_worker": 8,
            "id_issue": 16,
            "comment": "adipisicing aliqua ut esse qui",
            "created": 1381911306265,
            "time": 26945007
        },
        {
            "id": 44,
            "id_worker": 14,
            "id_issue": 0,
            "comment": "sit nulla",
            "created": 1177372256950,
            "time": 2269776
        },
        {
            "id": 45,
            "id_worker": 6,
            "id_issue": 23,
            "comment": "amet excepteur eu labore",
            "created": 1441521121483,
            "time": 8038415
        },
        {
            "id": 46,
            "id_worker": 0,
            "id_issue": 9,
            "comment": "reprehenderit veniam adipisicing et amet officia",
            "created": 1271784744744,
            "time": 25619693
        },
        {
            "id": 47,
            "id_worker": 13,
            "id_issue": 15,
            "comment": "commodo cupidatat commodo",
            "created": 1186537809791,
            "time": 28475357
        },
        {
            "id": 48,
            "id_worker": 4,
            "id_issue": 25,
            "comment": "do dolore",
            "created": 1245812896768,
            "time": 23023333
        },
        {
            "id": 49,
            "id_worker": 0,
            "id_issue": 22,
            "comment": "cillum non",
            "created": 1215456671729,
            "time": 20109756
        }
    ]
});
app.service('jsonWorkers', function jsonWorkers(){
    return [
        {
            "id": 0,
            "name": "Rogers",
            "surname": "Cox",
            "address": "Barwell Terrace",
            "created": 1455792791494
        },
        {
            "id": 1,
            "name": "Roberts",
            "surname": "Bates",
            "address": "Crosby Avenue",
            "created": 1239796377589
        },
        {
            "id": 2,
            "name": "Woodward",
            "surname": "Herring",
            "address": "Union Street",
            "created": 1465236430504
        },
        {
            "id": 3,
            "name": "Abbott",
            "surname": "Nichols",
            "address": "Lexington Avenue",
            "created": 1379485745451
        },
        {
            "id": 4,
            "name": "Isabelle",
            "surname": "Schultz",
            "address": "Clermont Avenue",
            "created": 1309315223988
        },
        {
            "id": 5,
            "name": "Hickman",
            "surname": "Sloan",
            "address": "President Street",
            "created": 1449224397914
        },
        {
            "id": 6,
            "name": "Olga",
            "surname": "Moore",
            "address": "Morgan Avenue",
            "created": 1309544045287
        },
        {
            "id": 7,
            "name": "Jasmine",
            "surname": "Fulton",
            "address": "Bergen Place",
            "created": 1409329026698
        },
        {
            "id": 8,
            "name": "Melton",
            "surname": "Hahn",
            "address": "Sharon Street",
            "created": 1192528015161
        },
        {
            "id": 9,
            "name": "Stevens",
            "surname": "Bender",
            "address": "Boerum Street",
            "created": 1271820223381
        },
        {
            "id": 10,
            "name": "Caroline",
            "surname": "Stokes",
            "address": "Nevins Street",
            "created": 1206777335030
        },
        {
            "id": 11,
            "name": "Jennie",
            "surname": "Lowery",
            "address": "Grimes Road",
            "created": 1371133808151
        },
        {
            "id": 12,
            "name": "Ann",
            "surname": "Cannon",
            "address": "Claver Place",
            "created": 1208895066012
        },
        {
            "id": 13,
            "name": "Washington",
            "surname": "Tate",
            "address": "Quay Street",
            "created": 1232652629755
        },
        {
            "id": 14,
            "name": "Julie",
            "surname": "Bradley",
            "address": "Lafayette Avenue",
            "created": 1292383338227
        }
    ]
});
app.service('changeView', function changeView(workersService, issuesService){

    var view = {
        dashboard: true,
        worker: false,
        issue: false
    };

    function changeView(name){
        view.dashboard = false;
        view.worker = false;
        view.issue = false;

        view[name] = true;
    }

    var goTo = function(view, item){
        switch(view){
            case 'dashboard':
                changeView('dashboard');
                break;
            case 'worker':
                workersService.viewWorker(item);
                changeView('worker');
                break;
            case 'issue':
                issuesService.viewIssue(item);
                changeView('issue');
                break;
        }
    };

    return {
        view: view,
        goTo: goTo
    };
});
app.service('issuesService', function issuesService(api){

    var data = {
        list: createItemsList(),
        issueWorkers: {},
        issueLogs: {},
        // active issue for view
        active: {}
    };

    function createItemsList(){
        return api.getData('issues').map(function(v){
            v.timeWorked = api.calcLogTime('id_issue', v.id);
            return v;
        });
    }

    function createIssueTable(item){
        data.issueWorkers = {};
        var itemsLog = api.getItemsBy('logs', 'id_issue', item.id);
        var items = {};

        itemsLog.forEach(function(v){
            // zawsze bedzie jeden element
            var el = api.getItemsBy('workers', 'id', v.id_worker)[0];
            if(el) {
                if (items[el.id]) {
                    items[el.id].timeWorked += v.time;
                } else {
                    el.timeWorked = (el.timeWorked || 0) + v.time;
                    items[el.id] = el;
                }
            }
        });

        data.active = item;
        data.issueWorkers = items;
    }

    function createIssueLogs(item){
        data.issueLogs = api.getItemsBy('logs', 'id_issue', item.id).map(function(v){
            var w = api.getItemsBy('workers', 'id', v.id_worker)[0];
            v.name = w.id + ': ' + w.name + ' '+ w.surname;
            return v;
        });
    }

    return {
        data: data,
        viewIssue: function(item){
            createIssueTable(item);
            createIssueLogs(item);
        }
    }

});
app.service('logsService', function logsService(api){
    var data = api.logs;


    return {
        data: data
    }

});
app.service('sortable', function sortable(){
    return function(defaultName){
        return {
            type: defaultName,
            reverse: false,
            change: function (name) {
                this.type = name;
                this.reverse = !this.reverse;
            },
            class: function (name) {
                if (this.type == name) {
                    return this.reverse ? 'up' : 'down';
                }
                return '';
            }
        }
    }
});
app.service('workersService', function workersService(api){

    var data = {
        // workers list
        list: createItemsList(),
        // issues list for worker
        workerIssues: {},
        workerLogs: {},
        // active issue for view
        active: {}
    };

    function createItemsList(){
        return api.getData('workers').map(function(v){
            v.timeWorked = api.calcLogTime('id_worker', v.id);
            return v;
        });
    }

    function createWorkerIssues(item){
        data.workerIssues = {};
        var itemsLog = api.getItemsBy('logs', 'id_worker', item.id);
        var itemsIssue = {};

        itemsLog.forEach(function(v){
            // zawsze bedzie jeden element
            var el = api.getItemsBy('issues', 'id', v.id_issue)[0];
            if(el) {
                if (itemsIssue[el.id]) {
                    itemsIssue[el.id].timeWorked += v.time;
                } else {
                    el.timeWorked = (el.timeWorked || 0) + v.time;
                    itemsIssue[el.id] = el;
                }
            }
        });

        data.active = item;
        data.workerIssues = itemsIssue;
    }

    function createWorkerLogs(item){
        data.workerLogs = {};
        data.workerLogs = api.getItemsBy('logs', 'id_worker', item.id).map(function(v){
            var w = api.getItemsBy('issues', 'id', v.id_issue)[0];
            v.name = w.id + ': ' + w.name + ' ('+ w.status +')';
            return v;
        });
    }

    return {
        data: data,
        viewWorker: function(item){
            createWorkerIssues(item);
            createWorkerLogs(item);
        }
    }

});