# Project demo - work logs

### TECHNOLOGY
* gulp 
    * compile sass
    * merge js and css files
* angular
* bootstrap

### DIST
* All generated files (css, js), are pushed to git. To run this demo, you don`t have to generate files.

### SERVER
* This project don`t use server

### API 
* json info in src/frontend/scripts/model/generator.md

### RUN
* To run this project demo open index file located in dist/index.html