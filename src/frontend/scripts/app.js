'use strict';
var app = angular.module('WorkLogsApp', ['ngRoute']);

app.controller('myCtrl', function myCtrl($scope, workersService, issuesService, logsService, changeView) {
    $scope.view = changeView.view;
    $scope.goTo = changeView.goTo;

    $scope.workers = workersService;
    $scope.issues = issuesService;
    $scope.logs = logsService;

});