app.directive('issueTable', function(changeView, sortable) {
    return {
        template: '<table class="table table-hover">' +
            '<thead><tr>' +
                '<th class="sortable sort-{{sort.class(\'name\')}}" ng-click="sort.change(\'name\')">Name</th>' +
                '<th class="sortable sort-{{sort.class(\'created\')}}" ng-click="sort.change(\'created\')">Created</th>' +
                '<th class="sortable sort-{{sort.class(\'status\')}}" ng-click="sort.change(\'status\')">Status</th>' +
                '<th class="sortable sort-{{sort.class(\'timeWorked\')}}" ng-click="sort.change(\'timeWorked\')">Time worked</th>' +
            '</tr></thead>' +
            '<tbody>' +
                '<tr ng-repeat="issue in list | orderObjectBy:sort.type:sort.reverse" ng-click="goTo(\'issue\', issue)" class="status-{{issue.status | diacritics}}">' +
                    '<td data-th="Name">{{issue.name}}</td>' +
                    '<td data-th="Created" class="created">{{issue.created | date : "yyyy-MM-dd"}}</td>' +
                    '<td data-th="Status" class="status"><span>{{issue.status}}</span></td>' +
                    '<td data-th="Time worked">{{issue.timeWorked | duration}}</td>' +
                '</tr>' +
            '</tbody>' +
            '</table>',
        scope:{
            list: '='
        },
        link: function(scope, element, attr){
            scope.goTo = changeView.goTo;
            scope.sort = new sortable('name');
        }
    };
});