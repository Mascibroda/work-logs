app.directive('logTable', function(changeView, sortable) {
    return {
        template: '<table class="table">' +
            '<thead><tr>' +
                '<th class="sortable sort-{{sort.class(\'name\')}}" ng-click="sort.change(\'name\')">Name</th>' +
                '<th class="sortable sort-{{sort.class(\'comment\')}}" ng-click="sort.change(\'comment\')">Comment</th>' +
                '<th class="sortable sort-{{sort.class(\'created\')}}" ng-click="sort.change(\'created\')">Created</th>' +
                '<th class="sortable sort-{{sort.class(\'time\')}}" ng-click="sort.change(\'time\')">Time worked</th>' +
            '</tr></thead>' +
            '<tbody>' +
                '<tr ng-repeat="item in list | orderBy:sort.type:sort.reverse">' +
                    '<td data-th="Name">{{item.name}}</td>' +
                    '<td data-th="Comment" title="{{item.comment}}">{{item.comment | limitTo : 20}}{{item.comment.length > 20 ? " ...":""}}</td>' +
                    '<td data-th="Created" class="created">{{item.created | date: "yyyy-MM-dd"}}</td>' +
                    '<td data-th="Time worked">{{item.time | duration}}</td>' +
                '</tr>' +
            '</tbody>' +
            '</table>',
        scope:{
            list: '='
        },
        link: function(scope, element, attr){
            scope.goTo = changeView.goTo;
            scope.sort = sortable('name');
        }
    };
});