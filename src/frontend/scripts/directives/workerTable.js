app.directive('workerTable', function(changeView, sortable) {
    return {
        template: '<table class="table table-hover">' +
            '<thead><tr>' +
                '<th class="sortable sort-{{sort.class(\'name\')}}" ng-click="sort.change(\'name\')">Name</th>' +
                '<th class="sortable sort-{{sort.class(\'surname\')}}" ng-click="sort.change(\'surname\')">Surname</th>' +
                '<th class="sortable sort-{{sort.class(\'address\')}}" ng-click="sort.change(\'address\')">Address</th>' +
                '<th class="sortable sort-{{sort.class(\'timeWorked\')}}" ng-click="sort.change(\'timeWorked\')">Time worked</th>' +
            '</tr></thead>' +
            '<tbody>' +
                '<tr ng-repeat="worker in list | orderObjectBy:sort.type:sort.reverse" ng-click="goTo(\'worker\', worker)">' +
                    '<td data-th="Name">{{worker.name}}</td>' +
                    '<td data-th="Surname">{{worker.surname}}</td>' +
                    '<td data-th="Address">{{worker.address}}</td>' +
                    '<td data-th="Time worked">{{worker.timeWorked | duration}}</td>' +
                '</tr>' +
            '</tbody>' +
            '</table>',
        scope:{
            list: '='
        },
        link: function(scope, element, attr){
            scope.goTo = changeView.goTo;
            scope.sort = sortable('name');
        }
    };
});