app.service('changeView', function changeView(workersService, issuesService){

    var view = {
        dashboard: true,
        worker: false,
        issue: false
    };

    function changeView(name){
        view.dashboard = false;
        view.worker = false;
        view.issue = false;

        view[name] = true;
    }

    var goTo = function(view, item){
        switch(view){
            case 'dashboard':
                changeView('dashboard');
                break;
            case 'worker':
                workersService.viewWorker(item);
                changeView('worker');
                break;
            case 'issue':
                issuesService.viewIssue(item);
                changeView('issue');
                break;
        }
    };

    return {
        view: view,
        goTo: goTo
    };
});