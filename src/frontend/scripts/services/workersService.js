app.service('workersService', function workersService(api){

    var data = {
        // workers list
        list: createItemsList(),
        // issues list for worker
        workerIssues: {},
        workerLogs: {},
        // active issue for view
        active: {}
    };

    function createItemsList(){
        return api.getData('workers').map(function(v){
            v.timeWorked = api.calcLogTime('id_worker', v.id);
            return v;
        });
    }

    function createWorkerIssues(item){
        data.workerIssues = {};
        var itemsLog = api.getItemsBy('logs', 'id_worker', item.id);
        var itemsIssue = {};

        itemsLog.forEach(function(v){
            // zawsze bedzie jeden element
            var el = api.getItemsBy('issues', 'id', v.id_issue)[0];
            if(el) {
                if (itemsIssue[el.id]) {
                    itemsIssue[el.id].timeWorked += v.time;
                } else {
                    el.timeWorked = (el.timeWorked || 0) + v.time;
                    itemsIssue[el.id] = el;
                }
            }
        });

        data.active = item;
        data.workerIssues = itemsIssue;
    }

    function createWorkerLogs(item){
        data.workerLogs = {};
        data.workerLogs = api.getItemsBy('logs', 'id_worker', item.id).map(function(v){
            var w = api.getItemsBy('issues', 'id', v.id_issue)[0];
            v.name = w.id + ': ' + w.name + ' ('+ w.status +')';
            return v;
        });
    }

    return {
        data: data,
        viewWorker: function(item){
            createWorkerIssues(item);
            createWorkerLogs(item);
        }
    }

});