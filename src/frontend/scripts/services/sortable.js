app.service('sortable', function sortable(){
    return function(defaultName){
        return {
            type: defaultName,
            reverse: false,
            change: function (name) {
                this.type = name;
                this.reverse = !this.reverse;
            },
            class: function (name) {
                if (this.type == name) {
                    return this.reverse ? 'up' : 'down';
                }
                return '';
            }
        }
    }
});