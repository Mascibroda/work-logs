app.service('issuesService', function issuesService(api){

    var data = {
        list: createItemsList(),
        issueWorkers: {},
        issueLogs: {},
        // active issue for view
        active: {}
    };

    function createItemsList(){
        return api.getData('issues').map(function(v){
            v.timeWorked = api.calcLogTime('id_issue', v.id);
            return v;
        });
    }

    function createIssueTable(item){
        data.issueWorkers = {};
        var itemsLog = api.getItemsBy('logs', 'id_issue', item.id);
        var items = {};

        itemsLog.forEach(function(v){
            // zawsze bedzie jeden element
            var el = api.getItemsBy('workers', 'id', v.id_worker)[0];
            if(el) {
                if (items[el.id]) {
                    items[el.id].timeWorked += v.time;
                } else {
                    el.timeWorked = (el.timeWorked || 0) + v.time;
                    items[el.id] = el;
                }
            }
        });

        data.active = item;
        data.issueWorkers = items;
    }

    function createIssueLogs(item){
        data.issueLogs = api.getItemsBy('logs', 'id_issue', item.id).map(function(v){
            var w = api.getItemsBy('workers', 'id', v.id_worker)[0];
            v.name = w.id + ': ' + w.name + ' '+ w.surname;
            return v;
        });
    }

    return {
        data: data,
        viewIssue: function(item){
            createIssueTable(item);
            createIssueLogs(item);
        }
    }

});