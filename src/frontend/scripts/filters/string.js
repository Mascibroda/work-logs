app.filter('diacritics', function () {
    return function (input) {
        return input.replace(/\s/g, '');
    };
});