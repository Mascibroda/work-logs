app.service('jsonIssues', function jsonIssues(){
    return [
        {
            "id": 0,
            "name": "consequat esse",
            "status": "todo",
            "created": 1269544866552
        },
        {
            "id": 1,
            "name": "proident dolore",
            "status": "done",
            "created": 1375883842162
        },
        {
            "id": 2,
            "name": "nulla ipsum",
            "status": "closed",
            "created": 1458093988099
        },
        {
            "id": 3,
            "name": "sint eiusmod",
            "status": "new",
            "created": 1468281959098
        },
        {
            "id": 4,
            "name": "cupidatat incididunt",
            "status": "closed",
            "created": 1331122056037
        },
        {
            "id": 5,
            "name": "ex sit",
            "status": "in progress",
            "created": 1358605915956
        },
        {
            "id": 6,
            "name": "minim aliquip",
            "status": "in progress",
            "created": 1393195918799
        },
        {
            "id": 7,
            "name": "sint proident",
            "status": "new",
            "created": 1378672376454
        },
        {
            "id": 8,
            "name": "eu aute",
            "status": "new",
            "created": 1353644182883
        },
        {
            "id": 9,
            "name": "elit aliquip",
            "status": "done",
            "created": 1379176791996
        },
        {
            "id": 10,
            "name": "sunt exercitation",
            "status": "todo",
            "created": 1428696592593
        },
        {
            "id": 11,
            "name": "ipsum dolore",
            "status": "closed",
            "created": 1456367454130
        },
        {
            "id": 12,
            "name": "in incididunt",
            "status": "in progress",
            "created": 1286270037573
        },
        {
            "id": 13,
            "name": "commodo ullamco",
            "status": "todo",
            "created": 1445349400712
        },
        {
            "id": 14,
            "name": "non sunt",
            "status": "new",
            "created": 1190659915364
        },
        {
            "id": 15,
            "name": "anim velit",
            "status": "in progress",
            "created": 1245838442429
        },
        {
            "id": 16,
            "name": "irure dolore",
            "status": "new",
            "created": 1211094800890
        },
        {
            "id": 17,
            "name": "sunt mollit",
            "status": "in progress",
            "created": 1200697726217
        },
        {
            "id": 18,
            "name": "excepteur cupidatat",
            "status": "done",
            "created": 1217538709434
        },
        {
            "id": 19,
            "name": "quis aute",
            "status": "todo",
            "created": 1246857345365
        },
        {
            "id": 20,
            "name": "cillum Lorem",
            "status": "closed",
            "created": 1232673543934
        },
        {
            "id": 21,
            "name": "ullamco deserunt",
            "status": "done",
            "created": 1416995638926
        },
        {
            "id": 22,
            "name": "excepteur incididunt",
            "status": "closed",
            "created": 1281576351768
        },
        {
            "id": 23,
            "name": "sit sunt",
            "status": "in progress",
            "created": 1371408462981
        },
        {
            "id": 24,
            "name": "commodo enim",
            "status": "new",
            "created": 1206556979276
        },
        {
            "id": 25,
            "name": "proident tempor",
            "status": "new",
            "created": 1304943998284
        },
        {
            "id": 26,
            "name": "deserunt labore",
            "status": "new",
            "created": 1347884243409
        },
        {
            "id": 27,
            "name": "sit ipsum",
            "status": "closed",
            "created": 1183690342483
        },
        {
            "id": 28,
            "name": "in eu",
            "status": "todo",
            "created": 1462295489469
        },
        {
            "id": 29,
            "name": "ex est",
            "status": "todo",
            "created": 1383755531234
        }
    ]
});