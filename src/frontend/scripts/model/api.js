app.service('api', function api(jsonWorkers, jsonLogs, jsonIssues){
    var data = {
        workers: jsonWorkers,
        issues: jsonIssues,
        logs: jsonLogs
    };

    function getItemsBy(table, col, value){
        var list = angular.copy(data[table]);
        var result = [];
        for(var i=0;i<list.length;i++){
            var el = list[i];
            if(typeof el[col] != 'undefined' && el[col] == value){
                result.push(el);
            }
        }
        return result;
    }

    function calcLogTime(col, value){
        var sum = 0;
        for(var i=0;i<data.logs.length;i++){
            var el = data.logs[i];
            if(el[col] == value){
                sum += el.time;
            }
        }
        return sum;
    }

    return {
        getData: function(name){
            if(data[name]){
                return angular.copy(data[name]);
            }else{
                console.error('table "'+ name +'" not exist');
                return [];
            }
        },
        getItemsBy: getItemsBy,
        calcLogTime: calcLogTime
    }

});