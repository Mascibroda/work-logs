app.service('jsonWorkers', function jsonWorkers(){
    return [
        {
            "id": 0,
            "name": "Rogers",
            "surname": "Cox",
            "address": "Barwell Terrace",
            "created": 1455792791494
        },
        {
            "id": 1,
            "name": "Roberts",
            "surname": "Bates",
            "address": "Crosby Avenue",
            "created": 1239796377589
        },
        {
            "id": 2,
            "name": "Woodward",
            "surname": "Herring",
            "address": "Union Street",
            "created": 1465236430504
        },
        {
            "id": 3,
            "name": "Abbott",
            "surname": "Nichols",
            "address": "Lexington Avenue",
            "created": 1379485745451
        },
        {
            "id": 4,
            "name": "Isabelle",
            "surname": "Schultz",
            "address": "Clermont Avenue",
            "created": 1309315223988
        },
        {
            "id": 5,
            "name": "Hickman",
            "surname": "Sloan",
            "address": "President Street",
            "created": 1449224397914
        },
        {
            "id": 6,
            "name": "Olga",
            "surname": "Moore",
            "address": "Morgan Avenue",
            "created": 1309544045287
        },
        {
            "id": 7,
            "name": "Jasmine",
            "surname": "Fulton",
            "address": "Bergen Place",
            "created": 1409329026698
        },
        {
            "id": 8,
            "name": "Melton",
            "surname": "Hahn",
            "address": "Sharon Street",
            "created": 1192528015161
        },
        {
            "id": 9,
            "name": "Stevens",
            "surname": "Bender",
            "address": "Boerum Street",
            "created": 1271820223381
        },
        {
            "id": 10,
            "name": "Caroline",
            "surname": "Stokes",
            "address": "Nevins Street",
            "created": 1206777335030
        },
        {
            "id": 11,
            "name": "Jennie",
            "surname": "Lowery",
            "address": "Grimes Road",
            "created": 1371133808151
        },
        {
            "id": 12,
            "name": "Ann",
            "surname": "Cannon",
            "address": "Claver Place",
            "created": 1208895066012
        },
        {
            "id": 13,
            "name": "Washington",
            "surname": "Tate",
            "address": "Quay Street",
            "created": 1232652629755
        },
        {
            "id": 14,
            "name": "Julie",
            "surname": "Bradley",
            "address": "Lafayette Avenue",
            "created": 1292383338227
        }
    ]
});