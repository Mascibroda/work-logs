http://www.json-generator.com/

ISSUES
------
[
  '{{repeat(30)}}',
  {
    id: '{{index()}}',
    name: '{{lorem(2, "words")}}',
 	status: function (tags) {
      var fruits = ['in progress', 'new', 'todo', 'closed', 'done'];
      return fruits[tags.integer(0, fruits.length - 1)];
    },
    created: '{{integer(1170075732175, new Date().getTime())}}'
  }
]

WORKERS
-------
[
  '{{repeat(15)}}',
  {
    id: '{{index()}}',
    name: '{{firstName()}}',
    surname: '{{surname()}}',
    address: '{{city() , street()}}',
    created: '{{integer(1170075732175, new Date().getTime())}}'
  }
]

LOGS
----
[
  '{{repeat(50)}}',
  {
    id: '{{index()}}',
    id_worker: '{{integer(0, 14)}}',
    id_issue: '{{integer(0, 29)}}',
    comment: '{{lorem(integer(2, 6), "words")}}',
    created: '{{integer(1170075732175, new Date().getTime())}}',
    time: '{{integer(6500, 2880000)}}'
  }
  
]