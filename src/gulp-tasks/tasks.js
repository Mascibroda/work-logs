module.exports = function (gulp, plugin, conf) {
	gulp.task('styles', function () {
		return gulp.src(conf.src.styles + '/**/*.scss')
			.pipe(plugin.plumber({errorHandler: function(err){plugin.onError(err,'styles')}}))
			.pipe(plugin.gulpif(!plugin.argv.dev,plugin.sourcemaps.init()))
			.pipe(plugin.sass({
				onError: function (e) {
					console.log(e);
				},
				outputStyle: plugin.argv.dev ? 'nested':'compressed'
			}))
			.pipe(plugin.autoprefixer({
				browsers: ['last 2 versions', 'ie >= 10'],
				cascade: false
			}))
			.pipe(plugin.gulpif(!plugin.argv.dev,plugin.sourcemaps.write(conf.maps)))
			.pipe(gulp.dest(conf.styles));
	});

	gulp.task('scripts', function () {
		return gulp.src([
            conf.src.scripts + '/app.js',
            conf.src.scripts + '/**/*.js'
        ])
			.pipe(plugin.plumber({errorHandler: function(err){plugin.onError(err,'scripts')}}))
			.pipe(plugin.concat('script.full.js'))
			.pipe(plugin.gulpif(!plugin.argv.dev,plugin.sourcemaps.init()))
			.pipe(plugin.rename('script.js'))
			.pipe(plugin.gulpif(!plugin.argv.dev,plugin.uglify({
				mangle: false
			})))
			.on('error', function(error){console.log('UGLIFY ERROR: ' + '[:' + error.lineNumber + '] - ' + error.message)})
			.pipe(plugin.gulpif(!plugin.argv.dev,plugin.sourcemaps.write(conf.maps)))
			.pipe(gulp.dest(conf.scripts));
	});

    gulp.task('plugins.styles', function () {
        return gulp.src(conf.src.plugins + '/**/*.css')
            .pipe(plugin.concat('plugins.css'))
            .pipe(gulp.dest(conf.styles));
    });

    gulp.task('plugins.scripts', function () {
        return gulp.src([
            conf.src.plugins + '/angular.min.js',
            conf.src.plugins + '/**/*.js'
        ])
            .pipe(plugin.concat('plugins.js'))
            .pipe(gulp.dest(conf.scripts));
    });
};