module.exports = function (gulp, plugin, conf) {

	/** WATCHERS **/
	gulp.task('watch.scripts', function() {
		gulp.watch([
			conf.src.scripts + '/**/*.js',
			conf.src.plugins + '/**/*.js'
		],[
			'scripts', 'plugins.scripts'
		]);
	});

	gulp.task('watch.styles', function() {
		gulp.watch([
            conf.src.styles + '/*.scss',
            conf.src.plugins + '/**/*.css'
        ],[
			'styles', 'plugins.styles'
		]);
	});
};