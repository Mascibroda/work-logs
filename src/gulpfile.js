var gulp        = require('gulp');
var plugin = {
	sass: require('gulp-sass'),
	sourcemaps: require('gulp-sourcemaps'),
	uglify: require('gulp-uglify'),
	concat: require('gulp-concat'),
	rename: require("gulp-rename"),
	gulpif: require('gulp-if'),
	expect: require('gulp-expect-file'),
	requireDir: require('require-dir'),
	notify: require('gulp-notify'),
	autoprefixer: require('gulp-autoprefixer'),
	plumber: require('gulp-plumber'),
	argv: require('yargs').argv,
	fs: require('fs'),
	onError: function (err,task) {
		var message='';
		if(err.message)message=err.message;
		if(err.file)message="Plik: "+err.file;
		if(err.fileName)message="Plik: "+err.fileName;
		if(message.length){
			message=message.substr(message.indexOf('frontend')+8);
		}
		plugin.notify.onError({
			title:    'Zadanie: '+task,
			message:  message,
			sound:    "Beep"
		})(err);
		console.log(err);
	}
};
var BASE = {
	app: '../dist/frontend',
	src: './frontend'
};
var conf = {
    styles: BASE.app + '/styles',
    scripts: BASE.app + '/scripts',
    // maps - relative path from styles or scripts dest dir
    maps: '../maps',
    src: {
        styles: BASE.src + '/styles',
        scripts: BASE.src + '/scripts',
        plugins: BASE.src + '/plugins'
    }
};

/*
	include files and set tasks
*/
generateTasks();

function generateTasks(files) {
	var files = files || plugin.requireDir('gulp-tasks', {recurse: true, duplicates: true});
	for (var file in files) {
		if (typeof files[file] == 'function'){
			files[file](gulp, plugin, conf);
		}else{
			generateTasks(files[file]);
		}
	}
}

gulp.task('build', ['styles', 'scripts', 'plugins.styles', 'plugins.scripts']);
gulp.task('watch', ['build', 'watch.styles', 'watch.scripts']);